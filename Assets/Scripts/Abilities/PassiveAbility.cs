﻿using Stats;
using SystemUsingAbility.Interface;
using UnityEngine;

namespace Abilities
{
    public abstract class PassiveAbility : ScriptableObject
    {
        [SerializeField] private int _cooldown;
        [SerializeField] private bool _isDebug;

        private IOwnerSystemUsingAbility _ownerSystemUsing;
        private SideStats _sideStats;

        private int _currentCooldown;
        private bool _isAbilityCompleted;

        private bool _isCooldownOver => _currentCooldown < 1;
        
        protected IOwnerSystemUsingAbility OwnerSystemUsing => _ownerSystemUsing;
        protected SideStats SideStats => _sideStats;

        public void Init(IOwnerSystemUsingAbility ownerSystemUsingAbility, SideStats sideStats)
        {
            _ownerSystemUsing = ownerSystemUsingAbility;
            _sideStats = sideStats;
            _currentCooldown = 0;
            Init();
        }
        
        public bool TryCast()
        {
            if (!IsCanUseAbility()) return false;
            
            _currentCooldown = _cooldown;
            Cast();
            return true;
        }
        
        public void RoundEnd()
        {
            _currentCooldown--;

            if (_isCooldownOver)
            {
                Complete();
            }
        }

        public void Complete()
        {
            ActionAfterAbilityCompleted();
        }
        
        private bool IsCanUseAbility()
        {
            if (_isDebug)
            {
                return true;
            }

            return _isCooldownOver && IsCanCast();
        }
        
        protected abstract void Init();
        protected abstract void Cast();
        protected abstract bool IsCanCast();
        protected abstract void ActionAfterAbilityCompleted();
        protected abstract void ActionAfterRoundEnd();
    }
}