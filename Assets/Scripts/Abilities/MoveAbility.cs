﻿using Cysharp.Threading.Tasks;
using DamageAcquisition;
using UnityEngine;

namespace Abilities
{
    public abstract class MoveAbility : ScriptableObject
    {
        public abstract UniTask Move(IDamageable target);
    }
}