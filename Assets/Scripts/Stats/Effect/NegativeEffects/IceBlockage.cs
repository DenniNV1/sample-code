using UnityEngine;

namespace Stats.Effect.NegativeEffects
{
    internal class IceBlockage : SideStatProviderDecorator
    {
        public override float Calculate()
        {
            return _sideStatProvider.Calculate() * 0.3f - 4f;
        }
    }
}
