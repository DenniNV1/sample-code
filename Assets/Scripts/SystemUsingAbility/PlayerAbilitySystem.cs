﻿using System;
using System.Collections.Generic;
using Abilities;
using Components.Grid;
using Components.Player.Movement;
using Cysharp.Threading.Tasks;
using DamageAcquisition;
using Stats;
using SystemUsingAbility.Interface;
using UnityEngine;

namespace SystemUsingAbility
{
    public class PlayerAbilitySystem : MonoBehaviour, IOwnerSystemUsingAbility
    {
        [SerializeField] private List<ActiveAbility> _activeAbilities;
        [SerializeField] private List<PassiveAbility> _passiveAbilities;

        private SideStats _sideStats;
        private SystemUsingActiveAbility _systemUsingActiveAbility;
        private PlayerSystemUsingMoveAbility _playerSystemUsingMoveAbility;
        private SystemUsingPassiveAbility _systemUsingPassiveAbility;
        private DamageAcquisitionSystem _damageAcquisitionSystem;

        private Action _useAbilitiesOver;
        private bool _isMovementActive;

        public SystemUsingActiveAbility SystemUsingActiveAbility => _systemUsingActiveAbility;
        public Vector3 Position => transform.position;

        public bool IsMovementActive => _isMovementActive;

        public void Init(SideStats sideStats, TurnBaseMovement turnBaseMovement, GridSegmentGenerator gridSegmentGenerator, DamageAcquisitionSystem damageAcquisitionSystem)
        {
            _sideStats = sideStats;
            _systemUsingActiveAbility = new SystemUsingActiveAbility(_activeAbilities, _sideStats.ActionPoints, this, _sideStats.Accuracy);
            _playerSystemUsingMoveAbility = new PlayerSystemUsingMoveAbility(sideStats.MoveActionPoints, turnBaseMovement, gridSegmentGenerator);
            _systemUsingPassiveAbility = new SystemUsingPassiveAbility(_passiveAbilities, this, _sideStats);
            _damageAcquisitionSystem = damageAcquisitionSystem;

            AddHandlers();
        }           

        public async UniTask TryCastActiveAbility(IDamageable damageable)
        {
            if (_isMovementActive) return;

           await _systemUsingActiveAbility.CurrentAbility.TryCast(damageable);
           
        }

        public void PointEnter(IDamageable damageable)
        {
            if (_isMovementActive) return;

            _systemUsingActiveAbility.CurrentAbility.PointEnter(damageable);
        }

        public void PointExit(IDamageable damageable)
        {
            if (_isMovementActive) return;

            _systemUsingActiveAbility.CurrentAbility.PointExit(damageable);
        }

        public void AddUseAbilitiesOverHandlers(Action useAbilitiesOver)
        {
            _useAbilitiesOver += useAbilitiesOver;
        }

        public void RemoveUseAbilitiesOverHandlers(Action useAbilitiesOver)
        {
            _useAbilitiesOver -= useAbilitiesOver;
        }

        public void ChangeCurrentAbility<TK>(TK ability) where TK : ActiveAbility
        {
            _systemUsingActiveAbility.ChangeCurrentAbility(ability);
        }
        
        public void ChangeCurrentAbility<TK>() where TK : ActiveAbility
        {
            _systemUsingActiveAbility.ChangeCurrentAbility<TK>();
        }

        public void RoundEnd()
        {
            _sideStats.RoundEnd();
            _systemUsingActiveAbility.RoundEnd();
            _systemUsingPassiveAbility.RoundEnd();
        }   

        public void BattleCompleted()
        {
            _playerSystemUsingMoveAbility.Stop();
            RemoveMoveDistanceOutline();
            _sideStats.BattleCompleted();
            _systemUsingPassiveAbility.BattleCompleted();
        }
        
        
        private void AddHandlers()
        {
            _systemUsingActiveAbility.AddAbilityCompleteHandler(AbilityCompletedHandler);
            _playerSystemUsingMoveAbility.AddAbilityCompleteHandler(AbilityCompletedHandler);
            _playerSystemUsingMoveAbility.AddHandlers();

            _damageAcquisitionSystem.DamageTaken += DamageTakenHandler;
        }

        private void RemoveHandlers()
        {
            _systemUsingActiveAbility.RemoveAbilityCompleteHandler(AbilityCompletedHandler);
            _playerSystemUsingMoveAbility.RemoveAbilityCompleteHandler(AbilityCompletedHandler);
            _playerSystemUsingMoveAbility.RemoveHandlers();
            _damageAcquisitionSystem.DamageTaken -= DamageTakenHandler;
        }


        private void OnDisable()
        {
            RemoveHandlers();
        }

        private void AbilityCompletedHandler()
        {
            if (_sideStats.ActionPoints.Value <= 0)
            {
                // _useAbilitiesOver?.Invoke();
            }
        }

        public void ChangeActiveAbilityOnMoveAbility()
        {
            _isMovementActive = true;
            _playerSystemUsingMoveAbility.Start();
        }

        public void ChangeMoveAbilityOnActiveAbility()
        {
            _isMovementActive = false;
            _playerSystemUsingMoveAbility.Stop();
        }

        public void RemoveMoveDistanceOutline()
        {
            _playerSystemUsingMoveAbility.RemoveOutline();
        }

        private void DamageTakenHandler(float damage)
        {
            foreach (var ability in SystemUsingActiveAbility.Abilities)
            {
                if (ability.UseAccelerator)
                {
                    ability.Accelerator += damage;
                }
            }
        }

    }
}